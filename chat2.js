// Load the http module to create an http server.
var http = require('http'); 
var spawn = require("child_process").spawn;

// Create a function to handle every HTTP request
function handler(req, res){
  var form = '';
  var message ;

  if(req.method == "GET"){ 
    form = '<!doctype html> \
            <html lang="en"> \
            <head> \
                <meta charset="UTF-8">  \
                <title>Form Calculator Add Example</title> \
            </head> \
            <body> \
              <form name="myForm" action="/" method="post">\
                  <input type="text" name="A"> + \
                  <span id="result"></span> \
                  <br> \
                  <input type="submit" value="Submit"> \
              </form> \
            </body> \
            </html>';

    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200);
    res.end(form);
  
  } 
  else if(req.method == 'POST'){
    req.on('data', function(chunk) {
      //grab form data as string
      var formdata = chunk.toString();
      formdata=formdata.replace("+"," ");
      formdata=formdata.replace("="," ");
      formdata=formdata.substring(2, formdata.length);
      var process = spawn('python',["chatbot.py", formdata]);

      process.stdout.on('data', function (chunk2){
      console.log(chunk2.toString())
      console.log(formdata)
   
       message=chunk2.toString()

      form = '<!doctype html> \
              <html lang="en"> \
              <head> \
                  <meta charset="UTF-8">  \
                  <title>Form Calculator Add Example</title> \
              </head> \
              <body> \
                <form name="myForm" action="/" method="post">\
                    <input type="text" name="A" value="'+formdata+'">  \
                    <span id="result">'+message+'</span> \
                    <br> \
                    <input type="submit" value="Submit"> \
                </form> \
              </body> \
              </html>';





    
      
    
      
    //respond
    res.setHeader('Content-Type', 'text/html');
    res.writeHead(200);
    res.end(form);


   });
    });

  } 
  else {
    res.writeHead(200);
    res.end();
  };

};


// Create a server that invokes the `handler` function upon receiving a request
http.createServer(handler).listen(8000, function(err){
  if(err){
    console.log('Error starting http server');
  } else {
    console.log("Server running at http://127.0.0.1:8000/ or http://localhost:8000/");
  };
});