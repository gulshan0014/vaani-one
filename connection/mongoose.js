
var mongoose = require('mongoose');
var dbPath  = `${process.env.mongoDb}/vaani?retryWrites=true&w=majority`;
mongoose.connect(dbPath);
// mongoose.connection.once('open', function() {
//   console.log("database connection open success");
// });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("mongoose connection open");
});

module.exports = db;