from chatterbot import ChatBot
from chatterbot.response_selection import get_most_frequent_response,get_first_response,get_random_response

#remove "read_only=true" statement to enable self learning
chatbot = ChatBot("Vaani", read_only=True,
                    storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
                    logic_adapters=[
                        {
                            'import_path': 'chatterbot.logic.SpecificResponseAdapter',
                            'input_text': 'i need help',
                            'output_text': 'how i can assist you ',
                            'input_text': 'Ayush Gangwar',
                            'output_text': 'He is one of my owner',
                            'input_text': 'Gulshan Kumar',
                            'output_text': ' my programmer ',
                        },
                        {
                            "import_path": "chatterbot.logic.BestMatch",
                            #"statement_comparison_function": "chatterbot.comparisons.sentiment_comparison",
                            "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                            #"statement_comparison_function": "chatterbot.comparisons.synset_distance",

                            "response_selection_method": "chatterbot.response_selection.get_most_frequent_response",
                            #"response_selection_method": "chatterbot.response_selection.get_first_response",
                            #"response_selection_method": "chatterbot.response_selection.get_random_response"
                        },
                        #   {
                        #       'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                        #       'threshold': 0.3,
                        #       'default_response': 'I am sorry, but I do not understand.'
                        #   }, # LowConfidenceAdapter in new version
                            {
                                'import_path': 'chatterbot.logic.BestMatch',
                                'default_response': 'I am sorry, but I do not understand.',
                                'maximum_similarity_threshold': 0.90

                            }
                    ],
                    preprocessors=[
                        'chatterbot.preprocessors.clean_whitespace'
                    ],
                    filters=[
                        'chatterbot.filters.RepetitiveResponseFilter'
                    ],
                    input_adapter='chatterbot.input.VariableInputTypeAdapter',
                    output_adapter='chatterbot.output.OutputAdapter',
                    database_uri = 'mongodb+srv://gulshan0014:asdfghjkl@cluster0-iapuw.mongodb.net/chatterbot-database?retryWrites=true&w=majority',
                    # database='chatterbot-database',
                    #twitter_consumer_secret=TWITTER["CONSUMER_SECRET"],
                    #twitter_access_token_key=TWITTER["ACCESS_TOKEN"],
                    #twitter_access_token_secret=TWITTER["ACCESS_TOKEN_SECRET"],
                    #trainer="chatterbot.trainers.TwitterTrainer"
                    )
