var mongoose = require('mongoose');
const db = require('../connection/mongoose');
mongoose.Promise = require('bluebird');

var userSchema = new  mongoose.Schema({
	firstName 	: {type:String,default:'',required:true},
	lastName 	: {type:String,default:'',required:true},
	email 		: {type:String,default:'',required:true},
	password 	: {type:String,default:'',required:true},
	created		: {type:Date},
	lastModified : {type:Date},
});


module.exports = db.model('User', userSchema);
