
var User = require('../models/userModel');

module.exports = (router) => {

    // router.get('/', function(req, res, next) {
    //     res.render('index', { title: 'Express' });
    // });

    router.get('/', function(req, res) {res.render('index');});
    router.get('/contact', function(req, res) {res.render('contact');});
    router.get('/about', function(req, res) {res.render('about');});
    router.get('/post', function(req, res) {res.render('post');});
    router.get('/registration', function(req, res) {res.render('registration_form');});
    router.get('/login', function(req, res) {res.render('login_form');});
    router.get('/profile', function(req, res) {
        sessCheck = req.session;
        email=req.session.email;
        var db =req.db;
        var registrationModel = req.registrationModel; // model name change to user
    
        if(sessCheck.email){
            User.find({'email':email},function(err,result){
    
                    if(err){
                       console.log("some error");
                    }
                    else{
                       console.log(result[0]['firstName']);
                       console.log(result[0]['lastName']);
                       f1 = result[0]['firstName'];
                       f2 = result[0]['lastName'];
    
                       res.render('profile',{user:email,firstName:f1,lastName:f2});
                    }
                 });
          
          //console.log(result['firstName']);
         }
         else{
          res.render("login_notification",{message:"You must login first."});
           
         }
    });

    router.get('/logout', function(req, res) {
        req.session.destroy(function(err){

            if(err){
                console.log(err);
            }else{
                res.render("login_notification",{message:"You are successfully logout."});
            }
        });
    
    });

    router.post('/registration',function(req, res) {

        var newUser = new User({
            firstName     : req.body.firstName,
            lastName      : req.body.lastName,
            email         : req.body.email,
            password      : req.body.password
        }); 
        var today = Date.now();
        newUser.created = today;

        User.find({'email': newUser.email},function(err,result){
        if(err){
            console.log("some error");
            res.send(err);
        }
        if(result[0]!= null){
            console.log(result);
            router.get('/registration_notification',function(req,res){
                    res.render("registration_notification",{message:"your are already Register to VAANI-ONE."})
            });

            res.redirect("registration_notification");
        }
        else{
            newUser.save(function(error){
                if(error){
                    console.log(error);
                    res.send(error);
                }
                else{
                    router.get('/registration_notification',function(req,res){
                        ss=newUser.firstName.toUpperCase();
                        ss2=newUser.lastName.toUpperCase();
                        res.render("registration_notification",{message:ss +" "+ss2+",your are successful Register to VAANI-ONE."})
                    });
                    res.redirect("registration_notification");
                }
            }); 
        }
    });




    
    });

    router.post('/login',function(req,res){
        var email = req.body.email;
        var password = req.body.password;
        User.find({'email':email,'password':password},function(err,result){
            if(err){
                console.log("some error");
                res.send(err);
            }
            if(result[0]!= null){
                sess = req.session;
                sess.email=email;
                res.redirect("profile");
            }
        else{
            router.get('/login_notification',function(req,res){
                res.render("login_notification",{message:"Email and password not match."});
            });
            res.redirect("login_notification");
        }


    });




    });
  return router;
}