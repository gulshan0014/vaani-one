require('dotenv').config()
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var spawn = require("child_process").spawn;
var bodyParser = require('body-parser');
var session = require('express-session');
app.use(bodyParser.json({limit:'10mb',extended:true}));
app.use(bodyParser.urlencoded({limit:'10mb',extended:true}));
app.use(session({secret: 'vaani'}));
var routes = require("./routes");

app.set('view engine', 'ejs');
app.use('/', routes(express.Router()));
app.use(express.static(__dirname + '/public'));
app.use(function(req,res,next){
    // req.db = db;
    // req.registrationModel = registrationModel;
    next();
});



io.on('connection', function(socket){
  console.log('A user connected');
  socket.on('userRequest', function (data) {
    console.log(data);

    var process = spawn('python',["chatbot.py", data]);
    process.stdout.on('data', function (chunk2){
      console.log(chunk2.toString());
      socket.emit('serverResponse',chunk2.toString());
    });
    //socket.emit('serverResponse',chunk2.toString());
  });
  socket.on('disconnect', function () {
    console.log('A user disconnected');
  });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});